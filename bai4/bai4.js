/**
 * input: 3 cạnh tam giác
 * 
 * progress: 
 * + lấy value của id firstEdge thông qua DOM và gán vào biến firstEdge
 * + lấy value của id secondEdge thông qua DOM và gán vào biến secondEdge
 * + lấy value của id thirdEdge thông qua DOM và gán vào biến thirdEdge
 * + tạo biến type = "Tam giác "
 * + tạo biến canhHuyen = Math.max(firstEdge, secondEdge, thirdEdge)
 * + kiểm tra
 *      + nếu firstEdge == secondEdge && secondEdge == thirdEdge => type += "Đều"
 *      + ngược lại nếu firstEdge == secondEdge || secondEdge == thirdEdge || firstEdge == thirdEdge => type += "Cân"
 *      + ngược lại nếu firstEdge * firstEdge + secondEdge * secondEdge == canhHuyen * canhHuyen => type = "Vuông"
 *      + ngược lại nếu firstEdge * firstEdge + thirdEdge * thirdEdge == canhHuyen * canhHuyen => type = "Vuông"
 *      + ngược lại => type = "Thường"
 * + lấy id result thông qua DOM
 * + Hiển thị type ra màn hình thông qua #result
 */

function typeOfTriangle() {
    var firstEdge = document.getElementById("firstEdge").value * 1;
    var secondEdge = document.getElementById("secondEdge").value * 1;
    var thirdEdge = document.getElementById("thirdEdge").value * 1;

    var type = "Tam giác ";
    var canhHuyen = Math.max(firstEdge, secondEdge, thirdEdge);

    if (firstEdge == secondEdge && secondEdge == thirdEdge) {
        type += "Đều"
    } else if (firstEdge == secondEdge || secondEdge == thirdEdge || firstEdge == thirdEdge) {
        type += "Cân"
    } else if (firstEdge * firstEdge + secondEdge * secondEdge == canhHuyen * canhHuyen) {
        type += "Vuông"
    } else if (firstEdge * firstEdge + thirdEdge * thirdEdge == canhHuyen * canhHuyen) {
        type += "Vuông"
    } else {
        type += "Thường"
    }

    document.getElementById("result").innerHTML = `
        <p>${type}</p>
    `
}