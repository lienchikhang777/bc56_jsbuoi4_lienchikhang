/**
 * input: 1 trong 4 thành viên
 * 
 * progess:
 * + tạo hàm greeting()
 * + lấy value của id person thông qua DOM và gán vào biến person
 * + tạo biến greeting = "Hello "
 * + sử dụng switch(person)
 * + nếu person = "B" thì greeting += "Bố"
 * + nếu person = "M" thì greeting += "Mẹ"
 * + nếu person = "A" thì greeting += "Anh trai"
 * + nếu person = "E" thì greeting += "Em gái"
 * + lấy id resualt thông qua DOM
 * + hiển thị greeting thông qua #result với phương thức innerHTML
 * 
 * output: lời chào thành viên
 */
function greeting() {
    var person = document.getElementById("person").value;
    var greeting = "Hello ";
    switch (person) {
        case "B":
            greeting += "Bố";
            break;
        case "M":
            greeting += "Mẹ";
            break;
        case "A":
            greeting += "Anh trai";
            break;
        case "E":
            greeting += "Em gái";
            break;
        default:
            break;
    }
    document.getElementById("result").innerHTML = `
        <p>${greeting}</p>
    `
}