/**
 * input: 3 số nguyên
 * 
 * progress:
 * + tạo hàm sort
 * + trong hàm sort:
 *      + lấy value của id first thông qua DOM và gán vào biến rank1
 *      + lấy value của id second thông qua DOM và gán vào biến rank2
 *      + lấy value của id third thông qua DOM và gán vào biến rank3
 *      + tạo biến temp = 0
 *      + nếu rank1 > rank2 thì đổi giá trị rank1 = rank2, rank2 = rank1 thông qua temp (trường hợp ngược lại "<=" thì giữ nguyên)
 *      + nếu rank2 > rank3 thì đổi giá trị rank2 = rank3, rank3 = rank2 thông qua temp (trường hợp ngược lại "<=" thì giữ nguyên)
 *          + sau khi hoán đổi thì sẽ có trường hợp rank1 > rank2 nên kiểm tra:
 *          + nếu rank1 > rank2 thì đổi giá trị rank1 = rank2, rank2 = rank1 thông qua temp (trường hợp ngược lại "<=" thì giữ nguyên)
 *      + lấy id result thông qua DOM
 *      + in ra kết quả rank1, rank2, rank3 thông qua div#result
 */

function sort() {
    var rank1 = document.getElementById("first").value * 1;
    var rank2 = document.getElementById("second").value * 1;
    var rank3 = document.getElementById("third").value * 1;

    var temp = 0;
    if (rank1 > rank2) {
        temp = rank1;
        rank1 = rank2;
        rank2 = temp;
    }

    if (rank2 > rank3) {
        temp = rank2;
        rank2 = rank3;
        rank3 = temp;
        if (rank2 < rank1) {
            temp = rank2;
            rank2 = rank1;
            rank1 = temp;
        }
    }

    document.getElementById("result").innerHTML = `
        <p>${rank1}, ${rank2}, ${rank3}</p>
    `
}