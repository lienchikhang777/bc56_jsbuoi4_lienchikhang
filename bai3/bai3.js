/**
 * input: 3 số nguyên
 * 
 * progress:
 * + tạo hàm isOddOrEven()
 * + lấy value của id first thông qua DOM và gán vào biến first
 * + lấy value của id second thông qua DOM và gán vào biến second
 * + lấy value của id third thông qua DOM và gán vào biến third
 * + tạo biến countOdd = 0
 * + tạo biến countEven = 0
 * + kiểm tra first
 *      nếu first % 2 == 0 => countEven++
 *      ngược lại => countOdd++
 * + kiểm tra second
 *      nếu second % 2 == 0 => countEven++
 *      ngược lại => countOdd+
 * + kiểm tra third
 *      nếu third % 2 == 0 => countEven++
 *      ngược lại => countOdd++
 * 
 * output: tổng số lẻ, số chẵn
 */
function isOddOrEven() {
    var first = document.getElementById("first").value * 1;
    var second = document.getElementById("second").value * 1;
    var third = document.getElementById("third").value * 1;
    var countOdd = 0;
    var countEven = 0;

    if (first % 2 == 0) {
        countEven++;
    } else {
        countOdd++;
    }

    if (second % 2 == 0) {
        countEven++;
    } else {
        countOdd++;
    }

    if (third % 2 == 0) {
        countEven++;
    } else {
        countOdd++;
    }

    document.getElementById("result").innerHTML = `
        <p>Odd number: ${countOdd}</p>
        <p>Even number: ${countEven}</p>
    `

}